# CUP Turnpoints Orderer

Tool for ordering turnpoints in SeeYou CUP file.

Source file is a valid CUP file. Target file is a (new) file with the same content, but turnpoints are ordered alphabetically.

Usage (Application Gradle Plugin):

```
./gradlew run --args="source-turnpoints.cup target-turnpoints.cup"
```
