package sk.smartware.cupturnpointsorderer

import java.io.File
import java.util.concurrent.Callable
import kotlin.system.exitProcess
import picocli.CommandLine

@CommandLine.Command(
    name = "java -jar cup-turnpoints-orderer-<version>-all.jar",
    mixinStandardHelpOptions = true
)
class Application : Callable<Int> {

    private val cupTurnpointsOrderer: CupTurnpointsOrderer = DefaultCupTurnpointsOrderer()

    @CommandLine.Parameters(
        index = "0",
        description = ["The source file path."]
    )
    lateinit var sourceFile: File

    @CommandLine.Parameters(
        index = "1",
        description = ["The target file path."]
    )
    lateinit var targetFile: File

    override fun call(): Int {
        println("Start.")
        cupTurnpointsOrderer.order(sourceFile, targetFile)
        println("Done.")
        return 0
    }
}

fun main(args: Array<String>) {
    val commandLine = CommandLine(Application())
    val status = commandLine.execute(*args)
    exitProcess(status)
}
