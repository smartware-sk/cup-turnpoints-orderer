package sk.smartware.cupturnpointsorderer

import java.io.File

interface CupTurnpointsOrderer {

    fun order(sourceFile: File, targetFile: File)
}
