package sk.smartware.cupturnpointsorderer

data class Turnpoint(
    val cupFileLine: String,
    val name: String,
    val hasNumericCode: Boolean,
    val remainingContent: String
) {

    fun formatAsCupFileLine(codeNumber: Int): String {
        return if (hasNumericCode) {
            val code = String.format("%03d", codeNumber)
            StringBuilder()
                .append("\"")
                .append(code)
                .append(name)
                .append("\",")
                .append(code)
                .append(",")
                .append(remainingContent)
                .toString()
        } else {
            cupFileLine
        }
    }

    companion object {

        private const val CODE_LENGTH = 3

        fun of(cupFileLine: String): Turnpoint {
            val cupFileLineParts = cupFileLine.split(",", limit = 3)
            val name = parseName(cupFileLineParts)
            val hasNumericCode = hasNumericCode(name)
            val alphabetPartOfName = if (hasNumericCode) {
                name.substring(CODE_LENGTH)
            } else {
                name
            }
            return Turnpoint(
                cupFileLine = cupFileLine,
                name = alphabetPartOfName,
                hasNumericCode = hasNumericCode,
                remainingContent = cupFileLineParts[2]
            )
        }

        private fun parseName(cupFileLineParts: List<String>): String {
            val rawName = cupFileLineParts[0]
            return rawName.removeSurrounding("\"")
        }

        private fun hasNumericCode(turnpointName: String): Boolean {
            val potentialNumberPartName = turnpointName.substring(0, CODE_LENGTH)
            return isNumericCode(potentialNumberPartName)
        }

        private fun isNumericCode(string: String): Boolean {
            return string.toIntOrNull() != null
        }
    }
}
