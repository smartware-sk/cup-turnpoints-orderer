package sk.smartware.cupturnpointsorderer

class TurnpointComparator : Comparator<Turnpoint> {

    override fun compare(turnpoint1: Turnpoint, turnpoint2: Turnpoint): Int {
        return when {
            turnpoint1.hasNumericCode && !turnpoint2.hasNumericCode -> -1
            !turnpoint1.hasNumericCode && turnpoint2.hasNumericCode -> 1
            else -> turnpoint1.name.compareTo(turnpoint2.name)
        }
    }
}
