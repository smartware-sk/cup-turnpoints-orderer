package sk.smartware.cupturnpointsorderer

import java.io.File

class DefaultCupTurnpointsOrderer : CupTurnpointsOrderer {

    companion object {
        private const val FIXED_LINES_FROM_TOP = 4
    }

    override fun order(sourceFile: File, targetFile: File) {
        val sourceLines = sourceFile.readLines()
        val targetLines = orderLines(sourceLines)
        printLinesToFile(targetLines, targetFile)
    }

    private fun orderLines(sourceLines: List<String>): List<String> {
        val fixedLinesFromTop = filterFixedLinesFromTop(sourceLines)
        val orderedTurnpointsAsLines = orderVariableLines(sourceLines)
        return fixedLinesFromTop + orderedTurnpointsAsLines
    }

    private fun filterFixedLinesFromTop(lines: List<String>): List<String> {
        return lines.subList(0, FIXED_LINES_FROM_TOP)
    }

    private fun orderVariableLines(sourceLines: List<String>): List<String> {
        val turnpointsToOrder = parseTurnpointsToOrder(sourceLines)
        val orderedTurnpoints = turnpointsToOrder.sortedWith(TurnpointComparator())
        return mapTurnpointsToCupFileLines(orderedTurnpoints)
    }

    private fun parseTurnpointsToOrder(lines: List<String>): List<Turnpoint> {
        return lines
            .filterIndexed { index, _ -> index >= FIXED_LINES_FROM_TOP }
            .map(Turnpoint.Companion::of)
    }

    private fun mapTurnpointsToCupFileLines(turnpoints: List<Turnpoint>): List<String> {
        return turnpoints.mapIndexed { index, turnpoint ->
            val codeNumber = index + FIXED_LINES_FROM_TOP
            turnpoint.formatAsCupFileLine(codeNumber)
        }
    }

    private fun printLinesToFile(lines: List<String>, file: File) {
        file.printWriter().use { printWriter ->
            lines.forEach { line ->
                printWriter.println(line)
            }
        }
    }
}
